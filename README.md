# Multimode GitLab-Runner

This runner will start a job baremetal in a SLURM dispatched job. Slurm variables should be set
to specify the number of nodes, tasks pers node and cpus per task.
See the [slurm-sarus runner](https://gitlab.com/cscs-ci/gitlab-runner-slur-sarus) for more information.

