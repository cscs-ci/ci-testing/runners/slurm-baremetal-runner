#!/bin/bash

CURRENT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
source "$CURRENT_DIR/../etc/defaults.sh"
source "$CURRENT_DIR/common.sh"

NAME="$CUSTOM_ENV_CI_COMMIT_SHORT_SHA-$CUSTOM_ENV_CI_JOB_ID"
JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

echo -e "Deallocating resources of ${b}${JOBID}${x} with name ${b}${NAME}${x}"

if [[ -n "$JOBID" ]]; then
    ( set -x ; scancel "$JOBID" 1>&2 )
fi

exit 0
