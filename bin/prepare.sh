#!/bin/bash

set -e
set -o pipefail

CURRENT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
source "$CURRENT_DIR/../etc/defaults.sh"
source "$CURRENT_DIR/common.sh"

# Allocate resources
source "$CURRENT_DIR/setup_slurm.sh" YES

# If a custom name is given, use that.
NAME="$CUSTOM_ENV_CI_COMMIT_SHORT_SHA-$CUSTOM_ENV_CI_JOB_ID"

OLDJOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

[[ -v SLURM_TIMELIMIT ]]  && export SALLOC_TIMELIMIT="$SLURM_TIMELIMIT"
[[ -v SLURM_CONSTRAINT ]] && export SALLOC_CONSTRAINT="$SLURM_CONSTRAINT"
[[ -v SLURM_PARTITION ]]  && export SALLOC_PARTITION="$SLURM_PARTITION"

if [[ -z "$OLDJOBID" ]]; then
    echo -e "Allocating a job with name ${b}$NAME${x}. Allowed=$ALLOWED"

    start_alloc=$(date +%s)

    ( set -x ; salloc \
        --no-shell \
        --job-name="$NAME" \
        --nodes="$SLURM_JOB_NUM_NODES" )

    if [ $? -ne 0 ]; then
        echo "Could not allocate resources"
        exit "$BUILD_FAILURE_EXIT_CODE"
    fi

    JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

    end_alloc=$(date +%s)
    let waiting_alloc=($end_alloc - $start_alloc)
    echo "Waited $waiting_alloc seconds for allocation"

    # send job meta data to middleware
    if [[ -z "$CLUSTER_NAME" && -f /etc/xthostname ]] ; then
        CLUSTER_NAME=$(cat /etc/xthostname)
    fi
    JOB_META_DATA='{"mode":"slurm-baremetal","allocation_wait_time":'$waiting_alloc',"slurm_job_id":"'$JOBID'","machine":"'$CLUSTER_NAME'","num_nodes":'$SLURM_JOB_NUM_NODES'}'
    curl -s --retry 5 --retry-connrefused -H "Content-Type: application/json" --data-raw "${JOB_META_DATA}" "${CUSTOM_ENV_CSCS_CI_MW_URL}/redis/job?token=${CUSTOM_ENV_CI_JOB_TOKEN}&job_id=${CUSTOM_ENV_CI_JOB_ID}"
else
    echo -e "Using existing allocation ${b}${OLDJOBID}${x} with name ${b}${NAME}${x}"
fi

exit 0
