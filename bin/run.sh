#!/usr/bin/env bash

set -e
set -o pipefail

CURRENT_DIR=$(dirname $(realpath "${BASH_SOURCE[0]:-$0}"))
source "$CURRENT_DIR/../etc/defaults.sh"
source "$CURRENT_DIR/common.sh"

file="$1"
stage="$2"

[[ ! -x "$file" ]] && echo "File $file is not executable" && exit $BUILD_FAILURE_EXIT_CODE

# Add an option to disable the after script because slurm is so slow...
if [[ "${stage}" == "after_script" && "${CUSTOM_ENV_DISABLE_AFTER_SCRIPT}" == "YES" ]]; then
    exit 0
fi

if [[ "${stage}" == "build_script" || "${stage}" == "step_script" || "${stage}" == "after_script" ]]; then
    source "$CURRENT_DIR/setup_slurm.sh"

    # Reuse the named allocation if given, otherwise the default.
    NAME="$CUSTOM_ENV_CI_COMMIT_SHORT_SHA-$CUSTOM_ENV_CI_JOB_ID"

    if [[ "$CUSTOM_ENV_TIME" = "YES" ]]; then
        TIME="time"
    else
        TIME=""
    fi

    # Find the corresponding job id
    JOBID=$(squeue -h --name="$NAME" --format=%A --states="RUNNING" | head -n1)

    # Move the gitlab ci script into a folder that we can mount
    SCRIPT_NAME=$(basename "$1")_$(uuidgen)
    cp "$1" "${CUSTOM_ENV_CI_PROJECT_DIR}/$SCRIPT_NAME"

    # Run ci script through sarus with a clean environment
    "${CLEANENV[@]}" bash -lc "set -x ; srun \
        --chdir='${CUSTOM_ENV_CI_PROJECT_DIR}' \
        --jobid='${JOBID}' \
        $TIME \
        '${CUSTOM_ENV_CI_PROJECT_DIR}/$SCRIPT_NAME'"

    result=$?

    # Exit using the variable, to make the build as failure in GitLab CI.
    if [ "$result" -ne 0 ]; then
        exit "$BUILD_FAILURE_EXIT_CODE"
    fi
elif [[ "${stage}" == "get_sources" ]]; then
    # retry getting sources up to 3 times, deleting the whole folder in between retries
    retry "$1" 3 random 10 30 "rm -Rf '$CUSTOM_ENV_CI_BUILDS_DIR'"
else
  ${1}
fi

exit 0
